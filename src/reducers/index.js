import { configureStore } from "@reduxjs/toolkit";
import books from './bookReducers'

export const store = configureStore({
    reducer: {
        books: books,
    }
});
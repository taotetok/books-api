import BookItem from "../bookItem/bookItem";
import React from "react";

const BookList = (props) => {
    const category = props.category;

    return (

        < div className="book-list" >
            <h3 className="book-list__results">Found 0 results</h3>
            <div className="book-list__container">

                {category
                    ?
                    props.specialBooks.map((book, i) => {
                        return <BookItem
                            key={i}
                            image={book.volumeInfo.imageLinks.thumbnail}
                            category={book.volumeInfo.categories}
                            title={book.volumeInfo.title}
                            author={book.volumeInfo.authors}
                            date={book.volumeInfo.publishedDate}
                        />
                    })
                    :
                    props.books.map((book, i) => {
                        return <BookItem
                            key={i}
                            image={book.volumeInfo.imageLinks.thumbnail}
                            category={book.volumeInfo.categories}
                            title={book.volumeInfo.title}
                            author={book.volumeInfo.authors}
                            date={book.volumeInfo.publishedDate}
                        />
                    })
                }

            </div>
        </div >
    )
}

export default BookList;
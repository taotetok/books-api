

function BookItem(props) {

    return (
        <div className="book-item">
            <img src={props.image} alt="" className="book-item__image" />
            <span className="book-item__title">{props.title}</span>
            <span className="book-item__categories">{props.category}</span>
            <span className="book-item__author">{props.author}</span>
            <span className="book-item__date">{props.date === '0000' ? '' : props.date.substring(0, 4)}</span>

        </div>
    )
}

export default BookItem;
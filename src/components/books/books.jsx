import { useState } from "react";
import React from "react";
import SearchBar from "../searchBar/searchBar";
import Filters from "../filters/filters";
import BookList from "../bookList/bookList";
import request from "superagent";

const Books = () => {
    const [booksArr, setbooksArr] = useState([]);
    const [specialBooks, setSpecialBooks] = useState([]);
    const [searchField, setSearchField] = useState('');
    const [sort, setSort] = useState('');
    const [category, setCategory] = useState('');

    const searchBook = (e) => {
        e.preventDefault();
        request
            .get('https://www.googleapis.com/books/v1/volumes')
            .query({ q: searchField })
            .then((data) => {
                console.log(data.body.items[0].volumeInfo.categories[0]);
                setbooksArr(cleanData(data));
                setSpecialBooks(cleanData(data));

            })

    }

    let handleSort = (e) => {
        setSort(e.target.value)
    }

    let handleCategorised = (e) => {
        setCategory(e.target.value)
    }

    let cleanData = (data) => {
        const cleanedData = data.body.items.map((books) => {
            if (books.volumeInfo.hasOwnProperty('publishedDate') === false) {
                books.volumeInfo['publishedDate'] = "0000";
            } else if (books.volumeInfo.hasOwnProperty('imageLinks') === false) {
                books.volumeInfo['imageLinks'] = { thumbnail: "" };
            } else if (books.volumeInfo.hasOwnProperty('categories') === false) {
                books.volumeInfo['categories'] = "";
            }
            else if (books.volumeInfo.hasOwnProperty('categories') === true) {
                if (books.volumeInfo.categories[0].includes(',')) {
                    books.volumeInfo['categories'] = books.volumeInfo.categories[0].split(',', 1);
                }
            }
            else if (books.volumeInfo.hasOwnProperty('authors') === false) {
                books.volumeInfo['authors'] = "";
            }
            return books;
        })

        return cleanedData;
    }

    let handleSearch = (e) => {
        setSearchField(e.target.value);
    }

    const categoriesedBooks = specialBooks.filter((books) => {
        if (category == 'Art' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('Art');
        } else if (category == 'Biography' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('Biography & Autobiography');
        } else if (category == 'Computers' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('Computers');
        } else if (category == 'History' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('History');
        } else if (category == 'Medical' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('Medical');
        } else if (category == 'Poetry' && books.volumeInfo.categories !== '') {
            return books.volumeInfo.categories.includes('Poetry');
        } else if (category == 'All') {
            return books;
        }
    })

    const sortedBooks = booksArr.sort((a, b) => {
        if (sort === 'Newest') {
            return parseInt(b.volumeInfo.publishedDate.substring(0, 4)) - parseInt(a.volumeInfo.publishedDate.substring(0, 4));
        } else if (sort === 'Relevance') {
            return parseInt(a.volumeInfo.publishedDate.substring(0, 4)) - parseInt(b.volumeInfo.publishedDate.substring(0, 4));
        }
    })

    return (
        <>
            <SearchBar searchBook={searchBook} handleSearch={handleSearch} />
            <Filters handleSort={handleSort} handleCategorised={handleCategorised} />
            <BookList books={sortedBooks} specialBooks={categoriesedBooks} category={category} />
        </>
    )
}

export default Books;
import React from "react";

const Filters = (props) => {

    return (
        <div className="filters">
            <div className="filters__categories">
                <p className="">Categories</p>
                <select defaultValue='Categories' onChange={props.handleCategorised} className="filters__dropdown">
                    <option value="All">All</option>
                    <option value="Art">Art</option>
                    <option value="Biography">Biography</option>
                    <option value="Computers">Computers</option>
                    <option value="History">History</option>
                    <option value="Medical">Medical</option>
                    <option value="Poetry">Poetry</option>
                </select>
            </div>
            <div className="filters__sorting">
                <p className="">Sorting by</p>
                <select defaultValue='Sort' onChange={props.handleSort} className="filters__dropdown">
                    <option value="Relevance">Relevance</option>
                    <option value="Newest">Newest</option>
                </select>
            </div>

        </div>
    )
}

export default Filters;
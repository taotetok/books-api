import React from "react";

const SearchBar = (props) => {
    return (
        <form className="search-bar" onSubmit={props.searchBook}>
            <input onChange={props.handleSearch} type="text" className="search-bar__input" />
            <button className="search-bar__btn" type="submit">Search</button>
        </form>
    )
}

export default SearchBar;
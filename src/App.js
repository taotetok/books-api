
import Header from './components/header/header';
import Books from './components/books/books';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Books />
    </div>
  );
}

export default App;
